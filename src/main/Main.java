/**
 * 
 */
package main;


import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;


/**
 * @author christian
 */
public class Main {

  /**
   * @param args
   * @throws DecoderException
   * @throws NoSuchPaddingException
   * @throws NoSuchAlgorithmException
   * @throws InvalidKeyException
   * @throws IOException
   */
  public static void main(String[] args) throws DecoderException, NoSuchAlgorithmException, NoSuchPaddingException,
          InvalidKeyException, IOException {
    byte[] key = Hex.decodeHex(new char[] {'c', 'd', '1', 'd', 'c', 'e', '0', '6', '1', 'f', '0', 'e', 'f', 'a', 'c',
        '1', 'e', '4', '5', '7', '4', '6', '0', 'd', '7', 'd', '6', 'c', '0', '9', 'f', '6'});

    for (int i = 1; i < 268; i++) {
      System.out.println("processing: " + i + "...");
      @SuppressWarnings("resource")
      DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream(Integer.toString(i))));
      ByteArrayOutputStream baos = new ByteArrayOutputStream();

      byte[] b = new byte[255];
      while (dis.read(b) != -1) {
        baos.write(b);
      }

      SecretKeySpec rc4Key = new SecretKeySpec(key, "RC4");

      // System.out.println("clear (ascii)        " + clearText);
      // System.out.println("clear (hex)          " + DatatypeConverter.printHexBinary(clearText.getBytes("ASCII")));
      // System.out.println("cipher (hex) is      " + DatatypeConverter.printHexBinary(cipherText));

      Cipher rc4Decrypt = Cipher.getInstance("RC4");
      rc4Decrypt.init(Cipher.DECRYPT_MODE, rc4Key);
      byte[] clearText2 = rc4Decrypt.update(baos.toByteArray());

      @SuppressWarnings("resource")
      FileOutputStream fos = new FileOutputStream("book/" + Integer.toString(i) + ".swf");
      fos.write(clearText2);
    }
  }
}
